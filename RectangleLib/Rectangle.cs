﻿
namespace RectangleLib
{
    public class Rectangle
    {
        public ICoordinate topLeftVertex { get; set; }
        public ICoordinate topRightVertex { get; set; }
        public ICoordinate bottomRightVertex { get; set; }
        public ICoordinate bottomLeftVertex { get; set; }

        public Rectangle(ICoordinate topLeft, ICoordinate topRight, ICoordinate bottomRight, ICoordinate bottomLeft)
        {
            this.topLeftVertex = topLeft;
            this.topRightVertex = topRight;
            this.bottomRightVertex = bottomRight;
            this.bottomLeftVertex = bottomLeft;
        }

        private double calculateDistanceByCoordinates(ICoordinate firstVertex, ICoordinate secondVertex)
        {
            return Math.Sqrt(Math.Pow(secondVertex.x - firstVertex.x, 2) + Math.Pow(secondVertex.y - firstVertex.y, 2));
        }

        public double calculateArea()
        {
            double leftLength = this.calculateDistanceByCoordinates(this.bottomLeftVertex, this.topLeftVertex);
            double topLength = this.calculateDistanceByCoordinates(this.topLeftVertex, this.topRightVertex);

            return leftLength * topLength;
        }

        public double calculatePerimeter()
        {
            double leftLength = this.calculateDistanceByCoordinates(this.bottomLeftVertex, this.topLeftVertex);
            double topLength = this.calculateDistanceByCoordinates(this.topLeftVertex, this.topRightVertex);
            double rightLength = this.calculateDistanceByCoordinates(this.topRightVertex, this.bottomRightVertex);
            double bottomLength = this.calculateDistanceByCoordinates(this.bottomRightVertex, this.bottomLeftVertex);

            return leftLength + topLength + rightLength + bottomLength;
        }

        public bool isSquare()
        {
            double leftLength = this.calculateDistanceByCoordinates(this.bottomLeftVertex, this.topLeftVertex);
            double topLength = this.calculateDistanceByCoordinates(this.topLeftVertex, this.topRightVertex);
            double rightLength = this.calculateDistanceByCoordinates(this.topRightVertex, this.bottomRightVertex);
            double bottomLength = this.calculateDistanceByCoordinates(this.bottomRightVertex, this.bottomLeftVertex);

            var restVertexesLength = new List<double> { topLength, rightLength, bottomLength };

            foreach (double vertexLength in restVertexesLength)
            {
                if (vertexLength != leftLength)
                {
                    return false;
                }
            };

            return true;
        }

        public bool isFirstQudrant()
        {
            var vertexes = new List<ICoordinate> { this.bottomLeftVertex, this.topLeftVertex, this.bottomRightVertex, this.bottomLeftVertex };

            foreach (ICoordinate vertex in vertexes)
            {
                if (!(vertex.x <= 0 && vertex.y >= 0))
                {
                    return false;
                }
            };

            return true;
        }
    }
}