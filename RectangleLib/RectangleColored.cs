﻿namespace RectangleLib
{
    public class RectangleColored : Rectangle
    {
        public string color { get; set; }
        public RectangleColored(
            ICoordinate topLeft,
            ICoordinate topRight,
            ICoordinate bottomRight,
            ICoordinate bottomLeft,
            string color
        ) : base (topLeft, topRight, bottomRight, bottomLeft) 
        {
            this.color = color;
        }

    }
}
