﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RectangleLib
{
    public interface ICoordinate
    {
        int x { get; set; }
        int y { get; set; }
    }
}
