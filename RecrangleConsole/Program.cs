﻿using RectangleLib;

namespace RectangleConsole
{
    class Coordinate : ICoordinate
    {
        public int x { get; set; }
        public int y { get; set; }

        public Coordinate(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

    }

    class Program
    {
        static string convertBoolToAnswer(bool value)
        {
            if (value) return "yes";

            return "no";
        }


        static void Main(string[] args)
        {
            Console.WriteLine("Enter coordinates of the rectangle. Use such format for setting coordinates: x,y. \n");

            Console.Write("Top left vertex: ");
            string[] topLeftXY = Console.ReadLine().Split(",");
            int topLeftX = Convert.ToInt32(topLeftXY[0]);
            int topLeftY = Convert.ToInt32(topLeftXY[1]);
            Console.Write("");


            Console.Write("Top right vertex: ");
            string[] topRightXY = Console.ReadLine().Split(",");
            int topRightX = Convert.ToInt32(topRightXY[0]);
            int topRightY = Convert.ToInt32(topRightXY[1]);
            Console.Write("");


            Console.Write("Bottom right vertex: ");
            string[] bottomRightXY = Console.ReadLine().Split(",");
            int bottomRightX = Convert.ToInt32(bottomRightXY[0]);
            int bottomRightY = Convert.ToInt32(bottomRightXY[1]);
            Console.Write("");


            Console.Write("Bottom left vertex: ");
            string[] bottomLeftXY = Console.ReadLine().Split(",");
            int bottomLeftX = Convert.ToInt32(bottomLeftXY[0]);
            int bottomLeftY = Convert.ToInt32(bottomLeftXY[1]);
            Console.Write("");

            Coordinate topLeftVertex = new Coordinate(topLeftX, topLeftY);
            Coordinate topRightVertex = new Coordinate(topRightX, topRightY);
            Coordinate bottomRightVertex = new Coordinate(bottomRightX, bottomRightY);
            Coordinate bottomLeftVertex = new Coordinate(bottomLeftX, bottomLeftY);


            Console.Write("Set color (y/n)?: ");
            string hasColor = Console.ReadLine();


            if (hasColor == "y")
            {
                Console.Write("Color: ");
                string color = Console.ReadLine();
                RectangleColored rectangle = new RectangleColored(topLeftVertex, topRightVertex, bottomRightVertex, bottomLeftVertex, color);

                string output = $"area: {rectangle.calculateArea()}\n" +
                              $"perimeter: {rectangle.calculatePerimeter()} \n" +
                              $"is it a square? {convertBoolToAnswer(rectangle.isSquare())} \n" +
                              $"is rectangle in first quadrant? {convertBoolToAnswer(rectangle.isFirstQudrant())} \n" +
                              $"color {rectangle.color} \n";

                Console.WriteLine($"\nRectangle info:  \n{output}");
            }
            else
            {
                Rectangle rectangle = new Rectangle(topLeftVertex, topRightVertex, bottomRightVertex, bottomLeftVertex);

                string output = $"area: {rectangle.calculateArea()}\n" +
                        $"perimeter: {rectangle.calculatePerimeter()} \n" +
                        $"is it a square? {convertBoolToAnswer(rectangle.isSquare())} \n" +
                        $"is rectangle in first quadrant? {convertBoolToAnswer(rectangle.isFirstQudrant())} \n";

                Console.WriteLine($"\nRectangle info:  \n{output}");
            }

            Console.ReadKey();
        }
    }
}